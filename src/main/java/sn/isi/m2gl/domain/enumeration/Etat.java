package sn.isi.m2gl.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    ACTIF, INACTIF
}
